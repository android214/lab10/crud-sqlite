import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart';

class DatabaseProvider {
  static Future<Database>? _database;
  static Future<Database> get database{
    return _database?? initDB();
  }
  static Future<Database> initDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _database = openDatabase(
      join(await getDatabasesPath(),'doggie_database.db'),
      onCreate: (db, version){
        return db.execute(
          'CREATE TABLE dogs(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER)'
        );
      },
      version: 1,
    );
    return _database as Future<Database>;
  }
}
