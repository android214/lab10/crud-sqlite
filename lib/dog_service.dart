import 'dog.dart';
import 'dog_dao.dart';

// var lastId = 3;
// var mockDogs = [
//   Dog(id: 1, name: 'A', age: 8),
//   Dog(id: 2, name: 'B', age: 5),
// ];

// int getNewId(){
//   return lastId++;
// }

Future<void> addNewDog(Dog dog){
  return DogDao.insertDog(dog);
}

Future<void> saveDog(Dog dog){
  return DogDao.updateDog(dog);
}

Future<void> delDog(Dog dog){
  return DogDao.deleteDog(dog.id);
}

Future<List<Dog>> getDogs() {
  return DogDao.dogs();
}
